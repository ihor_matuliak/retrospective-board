export class Task {
  constructor(
    public name: string,
    public likes: number,
    public comments: string[]
  ) {}
}
