import { Component, OnInit } from '@angular/core';
import { Board } from '../../models/board.model';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Column } from 'src/app/models/column.model';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-retrospective',
  templateUrl: './retrospective.component.html',
  styleUrls: ['./retrospective.component.scss'],
})
export class RetrospectiveComponent implements OnInit {
  columnName = '';
  taskName = '';
  commentText = '';

  board: Board = new Board('Board', [
    new Column('Mad', [
      new Task('Task 1', 0, ['Comment']),
      new Task('Task 2', 0, []),
      new Task('Task 3', 0, []),
    ]),
    new Column('Sad', [
      new Task('Task 4', 0, []),
      new Task('Task 5', 0, []),
      new Task('Task 6', 0, []),
    ]),
    new Column('Glad', [
      new Task('Task 7', 0, []),
      new Task('Task 8', 0, []),
      new Task('Task 9', 0, []),
    ]),
  ]);

  constructor() {}

  ngOnInit() {}

  viewInputTask(column: string) {
    this.columnName = column;
  }

  findTask(columnName, task) {
    return this.board.columns
      .find((c) => c.name === columnName)
      .tasks.find((i) => i === task);
  }

  createTask() {
    if (this.taskName.trim()) {
      this.board.columns
        .find((c) => c.name === this.columnName)
        .tasks.push(new Task(this.taskName, 0, []));

      this.taskName = this.columnName = '';
    } else {
      this.taskName = this.columnName = '';
    }
  }

  renameTask(column, item) {
    const newTaskName = prompt('Change task name');
    if (newTaskName.trim()) {
      this.findTask(column, item).name = newTaskName;
    } else {
      this.findTask(column, item).name = item.name;
    }
  }

  addLike(column, item) {
    this.findTask(column, item).likes++;
  }

  addComment(column, item) {
    if (this.commentText.trim()) {
      this.findTask(column, item).comments.push(this.commentText);
      this.taskName = this.commentText = '';
    } else {
      this.taskName = this.columnName = '';
    }
  }

  openCommentField(item) {
    this.taskName = item.name;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
}
